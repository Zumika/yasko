package controllers

import (
	"fmt"
	"yasko/config"
	"yasko/helpers"
	"yasko/logger"
	"yasko/models"
)

type userController struct{}

var Users = new(userController)

func (uc userController) Register(email string) (*models.User, error) {
	password:= helpers.GenerateToken("p")
	user, err := models.Users.Create(email, password)
	if err != nil {
		return nil, err
	}
	//Send email to user with password
	return user, nil
}

func (uc userController) Login(email, password string) (string, error) {
	var token string
	user, err := models.Users.ByEmail(email)
	if err != nil {
		logger.Info.Print("Get User error: ", err)
		return token, err
	}
	ok, err := uc.CheckPassword(user, password)
	if err != nil {
		logger.Info.Print("Check password: ", err)
		return token, nil
	}
	if ok {
		token, err = user.GenerateToken()
		if err != nil {
			logger.Info.Print("Generate token error: ", err)
			return token, err
		}
	}
	//Create key value in redis
	return token, err
}

func (uc userController) Logout() error {
	//Delete key value in redis
	return nil
}

func (uc userController) CheckPassword(u *models.User, password string) (bool, error) {
	passwordHash, err := helpers.GeneratePassword(password, config.SecretKey)
	if err != nil {
		return false, err
	}
	origin := fmt.Sprintf("%x", u.Password)
	last := fmt.Sprintf("%x", passwordHash)
	if origin == last {
		return true, nil
	}
	return false, nil
}
