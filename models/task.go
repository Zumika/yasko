package models

import (
	"gopkg.in/mgo.v2/bson"

	"yasko/datastore"
	"yasko/helpers"
)

type tasks struct{}

var Tasks = new(tasks)

type Task struct {
	ID            string  `bson:"_id"`
	UserID        string  `bson:"user_id"`
	Active        bool    `bson:"active"`
	Done          bool    `bson:"done"`
	Price         float64 `bson:"price"`
	PlatformID    string  `bson:"platform_id"`
	AuthorityID   string  `bson:"authority_id"`
	PlatformCount int     `bson:"platform_count"`
}

func (t tasks) Create(userID, platformID, authorityID string, price float64, platformCount int) (*Task, error) {
	task := &Task{
		ID:            helpers.GenerateID("task"),
		UserID:        userID,
		Active:        true,
		Done:          false,
		Price:         price,
		PlatformCount: platformCount,
		PlatformID:    platformID,
		AuthorityID:   authorityID,
	}
	return task, datastore.Mongo.Tasks.Insert(task)
}

func (t tasks) ByID(id string) (*Task, error) {
	var task *Task
	return task, datastore.Mongo.Tasks.FindId(id).One(&task)
}

func (t tasks) ByUserID(userID string) ([]*Task, error) {
	var tasks []*Task
	return tasks, datastore.Mongo.Users.Find(bson.M{
		"user_id": userID,
	}).All(&tasks)
}

func (t *Task) Update(active, done bool) error {
	update := bson.M{}
	if active != t.Active {
		update["active"] = active
		t.Active = active
	}
	if done != t.Done {
		update["done"] = done
		t.Done = done
	}
	return datastore.Mongo.Tasks.UpdateId(t.ID, bson.M{"$set": update})
}
