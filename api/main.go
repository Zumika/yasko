package api

import (
	"yasko/server"
)

// JSONError wraps an error type into a map
func JSONError(err error) interface{} {
	return map[string]string{"error": err.Error()}
}

// Start registers all handlers for the subroutes
func Start(r *server.Router) {
	r.OPTIONS("/{rest:.*}", corsPreflight)
	handleUser(r.Group("/users"))
	handlePlatform(r.Group("/platforms"))
	handleAuthority(r.Group("/authorities"))
	handleTask(r.Group("/tasks"))
	handleTheme(r.Group("/themes"))
	handleParameter(r.Group("/parameters"))
	r.Use(corsAfterflight)
}
