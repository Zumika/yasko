package server

import (
	"net/http"

	"github.com/gorilla/mux"
)

var middlewares = []func(*Context){}

// Router wraps the mus router
type Router struct {
	router *mux.Router
}

// GET registers a GET route
func (r *Router) GET(path string, handler func(*Context)) {
	r.route(path, "GET", handler)
}

// POST registers a POST route
func (r *Router) POST(path string, handler func(*Context)) {
	r.route(path, "POST", handler)
}

// PUT registers a PUT route
func (r *Router) PUT(path string, handler func(*Context)) {
	r.route(path, "PUT", handler)
}

// DELETE registers a DELETE route
func (r *Router) DELETE(path string, handler func(*Context)) {
	r.route(path, "DELETE", handler)
}

// OPTIONS registers a OPTIONS route
func (r *Router) OPTIONS(path string, handler func(*Context)) {
	r.route(path, "OPTIONS", handler)
}

func (r *Router) route(path, method string, handler func(*Context)) {
	h := func(w http.ResponseWriter, r *http.Request) {
		c := NewContext(w, r)
		for _, m := range middlewares {
			m(c)
		}
		handler(c)
	}

	if path == "" {
		r.router.Methods(method).HandlerFunc(h)
	} else {
		r.router.Methods(method).Path(path).HandlerFunc(h)
	}
}

// Static handles static content
func (r *Router) Static(pathPrefix, path string) {
	r.router.PathPrefix(pathPrefix).Handler(http.FileServer(http.Dir(path)))
}

// Group groups all routes into one group
func (r *Router) Group(pathPrefix string) *Router {
	return &Router{router: r.router.PathPrefix(pathPrefix).Subrouter()}
}

// Use adds a middleware to the list of middlewares
func (r *Router) Use(f func(*Context)) {
	middlewares = append(middlewares, f)
}
