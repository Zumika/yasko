package api

import (
	"net/http"

	"yasko/models"
	"yasko/server"
	"yasko/controllers"
)

var task = new(taskAPI)

type taskAPI struct{}

func handleTask(r *server.Router) {
	r.GET("", requireAuth(task.show))
	r.POST("", task.create)
	r.PUT("", task.update)
}

func (ta *taskAPI) show(c *server.Context, user *models.User) {
	objects, err := models.Tasks.ByUserID(user.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, ta.multiResponse(objects))
}

func (ta *taskAPI) create(c *server.Context) {
	var req createTaskRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := controllers.Tasks.Create(req.UserID, req.PlatformID, req.AuthorityID, req.ThemeID, req.Parameters, req.PlatformCount)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, ta.response(object))
}

func (ta *taskAPI) update(c *server.Context) {
	var req updateTaskRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Tasks.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Update(req.Active, req.Done); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, ta.response(object))
}

func (ta *taskAPI) multiResponse(tasks []*models.Task) []*taskResponse {
	var tr []*taskResponse
	for _, task := range tasks {
		resp := ta.response(task)
		if resp != nil {
			tr = append(tr, resp)
		}
	}
	return tr
}

func (ta *taskAPI) response(t *models.Task) *taskResponse {
	return &taskResponse{
		ID:            t.ID,
		UserID:        t.UserID,
		Active:        t.Active,
		Done:          t.Done,
		Price:         t.Price,
		PlatformID:    t.PlatformID,
		AuthorityID:   t.AuthorityID,
		PlatformCount: t.PlatformCount,
	}
}
