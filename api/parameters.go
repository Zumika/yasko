package api

import (
	"net/http"

	"yasko/models"
	"yasko/server"
)

var parameter = new(parameterAPI)

type parameterAPI struct{}

func handleParameter(r *server.Router) {
	r.GET("", parameter.show)
	r.POST("", parameter.create)
	r.PUT("", parameter.update)
	r.DELETE("", parameter.remove)
}

func (aa *parameterAPI) show(c *server.Context) {
	objects, err := models.Parameters.All()
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.multiResponse(objects))
}

func (aa *parameterAPI) create(c *server.Context) {
	var req createParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Parameters.Create(req.Title, req.Active, req.Price)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.response(object))
}

func (aa *parameterAPI) update(c *server.Context) {
	var req updateParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Parameters.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Update(req.Title, req.Active, req.Price); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.response(object))
}

func (aa *parameterAPI) remove(c *server.Context) {
	var req removeRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Parameters.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Remove(); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
}

func (aa *parameterAPI) multiResponse(parameters []*models.Parameter) []*parameterResponse {
	var pr []*parameterResponse
	for _, parameter := range parameters {
		resp := aa.response(parameter)
		if resp != nil {
			pr = append(pr, resp)
		}
	}
	return pr
}

func (aa *parameterAPI) response(p *models.Parameter) *parameterResponse {
	return &parameterResponse{
		ID:     p.ID,
		Title:  p.Title,
		Active: p.Active,
		Rate:   p.Rate,
	}
}
