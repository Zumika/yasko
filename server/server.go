package server

import (
	"net/http"

	"yasko/logger"

	"github.com/gorilla/mux"
)

// Server wraps a router
type Server struct {
	router *mux.Router
}

// ListenAndServe starts the HTTP
func (s *Server) ListenAndServe(bind string) error {
	logger.Info.Printf("API: Started server on %s", bind)
	return http.ListenAndServe(bind, s.logger(s.router))
}

// Host registers a router for the given host an path prefix
func (s *Server) Host(host, pathPrefix string) *Router {
	return &Router{router: s.router.PathPrefix(pathPrefix).Host(host).Subrouter()}
}

// NewServer returns a new
func NewServer() *Server {
	s := &Server{router: mux.NewRouter()}
	return s
}

func (s *Server) logger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler.ServeHTTP(w, r)
	})
}
