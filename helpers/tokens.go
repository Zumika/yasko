package helpers

import "github.com/rs/xid"

func GenerateToken(prefix string) string {
	id := xid.New().String()
	return prefix + id
}
