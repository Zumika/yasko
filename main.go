package main

import (
	"runtime"

	"yasko/api"
	"yasko/config"
	"yasko/server"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU() / 2)

	s := server.NewServer()
	api.Start(s.Host("localhost", "/v1"))
	err := s.ListenAndServe(config.Server.Address)
	if err != nil {
		panic(err)
	}
}
