package models

import (
	"fmt"
	"gopkg.in/mgo.v2/bson"

	"yasko/datastore"
	"yasko/helpers"
)

type platforms struct{}

var Platforms = new(platforms)

type Platform struct {
	ID           string  `bson:"_id"`
	Title        string  `bson:"title"`
	Availability bool    `bson:"availability"`
	Price        float64 `bson:"price"`
}

func (p Platform) String() string {
	return fmt.Sprintf("Platform{ID:%v, Title:%v, Availability:%v, Price:%v}", p.ID, p.Title, p.Availability, p.Price)
}

func (p platforms) Create(title string, availability bool, price float64) (*Platform, error) {
	platform := &Platform{
		ID:           helpers.GenerateID("platform"),
		Title:        title,
		Availability: availability,
		Price:        price,
	}
	return platform, datastore.Mongo.Platforms.Insert(platform)
}

func (p platforms) ByID(id string) (*Platform, error) {
	var platform *Platform
	return platform, datastore.Mongo.Platforms.FindId(id).One(&platform)
}

func (p platforms) All() ([]*Platform, error) {
	var platforms []*Platform
	return platforms, datastore.Mongo.Platforms.Find(bson.M{}).All(&platforms)
}

func (p *Platform) Update(title string, availability bool, price float64) error {
	update := bson.M{}
	if title != "" && title != p.Title {
		update["title"] = title
		p.Title = title
	}
	if availability != p.Availability {
		update["availability"] = availability
		p.Availability = availability
	}
	if price != p.Price {
		update["price"] = price
		p.Price = price
	}
	return datastore.Mongo.Platforms.UpdateId(p.ID, bson.M{"$set": update})
}

func (p *Platform) Remove() error {
	return datastore.Mongo.Platforms.RemoveId(p.ID)
}
