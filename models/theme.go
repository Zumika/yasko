package models

import (
	"fmt"
	"gopkg.in/mgo.v2/bson"

	"yasko/datastore"
	"yasko/helpers"
)

type themes struct{}

var Themes = new(themes)

type Theme struct {
	ID           string  `bson:"_id"`
	Title        string  `bson:"title"`
	Availability bool    `bson:"availability"`
	Rate         float64 `bson:"rate"`
}

func (a Theme) String() string {
	return fmt.Sprintf("Theme{ID:%v, Title:%v, Availability:%v, Price:%v}", a.ID, a.Title, a.Availability, a.Rate)
}

func (a themes) Create(title string, availability bool, rate float64) (*Theme, error) {
	theme := &Theme{
		ID:           helpers.GenerateID("theme"),
		Title:        title,
		Availability: availability,
		Rate:         rate,
	}
	return theme, datastore.Mongo.Themes.Insert(theme)
}

func (a themes) ByID(id string) (*Theme, error) {
	var theme *Theme
	return theme, datastore.Mongo.Themes.FindId(id).One(&theme)
}

func (a themes) All() ([]*Theme, error) {
	var themes []*Theme
	return themes, datastore.Mongo.Themes.Find(bson.M{}).All(&themes)
}

func (a *Theme) Update(title string, availability bool, rate float64) error {
	update := bson.M{}
	if title != "" && title != a.Title {
		update["title"] = title
		a.Title = title
	}
	if availability != a.Availability {
		update["availability"] = availability
		a.Availability = availability
	}
	if rate != a.Rate {
		update["rate"] = rate
		a.Rate = rate
	}
	return datastore.Mongo.Themes.UpdateId(a.ID, bson.M{"$set": update})
}

func (a *Theme) Remove() error {
	return datastore.Mongo.Themes.RemoveId(a.ID)
}
