package api

import (
	"errors"
	"regexp"

	"yasko/helpers"
)

var (
	nameRegex  = regexp.MustCompile(`(.){1,50}`)
	emailRegex = regexp.MustCompile(`^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$`)
	titleRegex = regexp.MustCompile(`[a-zA-Z_]{1,50}`)
)

type createUserRequest struct {
	Email string `form:"email" json:"email"`
}

func (r *createUserRequest) OK() error {
	if !emailRegex.MatchString(r.Email) {
		return errors.New("Invalid email")
	}
	return nil
}

type loginUserRequest struct {
	Email    string `form:"email" json:"email"`
	Password string `form:"password" json:"password"`
}

func (r *loginUserRequest) OK() error {
	if !emailRegex.MatchString(r.Email) {
		return errors.New("Invalid email")
	}
	if r.Password == "" {
		return errors.New("Password can not be empty")
	}
	return nil
}

type activateUserRequest struct {
	Token string
}

func (r *activateUserRequest) Parse(values map[string]string) error {
	r.Token = values["token"]
	if r.Token == "" {
		return errors.New("invalid token")
	}
	return nil
}

type createParameterRequest struct {
	Title        string  `form:"title" json:"title"`
	Availability bool    `form:"availability" json:"availability"`
	Active       bool    `form:"active" json:"active"`
	Price        float64 `form:"price" json:"price"`
}

func (r *createParameterRequest) OK() error {
	if !titleRegex.MatchString(r.Title) {
		return errors.New("Invalid title")
	}
	if len(r.Title) == 0 {
		return errors.New("Invalid title")
	}
	if r.Price < 0 {
		return errors.New("Invalid price")
	}
	return nil
}

type updateParameterRequest struct {
	ID           string  `form:"id" json:"id"`
	Title        string  `form:"title" json:"title"`
	Availability bool    `form:"availability" json:"availability"`
	Active       bool    `form:"active" json:"active"`
	Price        float64 `form:"price" json:"price"`
}

func (r *updateParameterRequest) OK() error {
	if !nameRegex.MatchString(r.ID) {
		return errors.New("Invalid id")
	}
	if len(r.ID) == 0 {
		return errors.New("ID can not be empty")
	}
	if !titleRegex.MatchString(r.Title) {
		return errors.New("Invalid title")
	}
	if r.Price < 0 {
		return errors.New("Invalid price")
	}
	return nil
}

type removeRequest struct {
	ID string `form:"id" json:"id"`
}

func (r *removeRequest) OK() error {
	if !nameRegex.MatchString(r.ID) {
		return errors.New("Invalid id")
	}
	if len(r.ID) == 0 {
		return errors.New("ID can not be empty")
	}
	return nil
}

type createTaskRequest struct {
	UserID        string          `json:"user_id"`
	PlatformID    string          `json:"platform_id"`
	AuthorityID   string          `json:"authority_id"`
	ThemeID       string          `json:"theme_id"`
	Parameters    []helpers.Parameter `json:"parameters"`
	PlatformCount int             `json:"platform_count"`
}

func (r *createTaskRequest) OK() error {
	if !nameRegex.MatchString(r.UserID) {
		return errors.New("Invalid user id")
	}
	if !nameRegex.MatchString(r.PlatformID) {
		return errors.New("Invalid platform id")
	}
	if !nameRegex.MatchString(r.AuthorityID) {
		return errors.New("Invalid platform id")
	}
	if r.PlatformCount < 1 {
		return errors.New("Invalid platform count")
	}
	return nil
}

type updateTaskRequest struct {
	ID     string `form:"id" json:"id"`
	Active bool   `form:"active" json:"active"`
	Done   bool   `form:"done" json:"done"`
}

func (r *updateTaskRequest) OK() error {
	if !nameRegex.MatchString(r.ID) {
		return errors.New("Invalid task id")
	}
	return nil
}
