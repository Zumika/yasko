package helpers

type Parameter struct {
	ID     string `form:"id" json:"id"`
	Active bool   `form:"active" json:"active"`
}
