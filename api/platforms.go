package api

import (
	"net/http"

	"yasko/models"
	"yasko/server"
)

var platform = new(platformAPI)

type platformAPI struct{}

func handlePlatform(r *server.Router) {
	r.GET("", platform.show)
	r.POST("", platform.create)
	r.PUT("", platform.update)
	r.DELETE("", platform.remove)
}

func (pa *platformAPI) show(c *server.Context) {
	platforms, err := models.Platforms.All()
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, pa.multiResponse(platforms))
}

func (pa *platformAPI) create(c *server.Context) {
	var req createParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	platform, err := models.Platforms.Create(req.Title, req.Availability, req.Price)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, pa.response(platform))
}

func (pa *platformAPI) update(c *server.Context) {
	var req updateParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	platform, err := models.Platforms.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := platform.Update(req.Title, req.Availability, req.Price); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, pa.response(platform))
}

func (pa *platformAPI) remove(c *server.Context) {
	var req removeRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	platform, err := models.Platforms.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := platform.Remove(); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
}

func (pa *platformAPI) multiResponse(platforms []*models.Platform) []*parameterResponse {
	var pr []*parameterResponse
	for _, platform := range platforms {
		resp := pa.response(platform)
		if resp != nil {
			pr = append(pr, resp)
		}
	}
	return pr
}

func (pa *platformAPI) response(p *models.Platform) *parameterResponse {
	return &parameterResponse{
		ID:           p.ID,
		Title:        p.Title,
		Availability: p.Availability,
		Price:        p.Price,
	}
}
