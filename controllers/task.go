package controllers

import (
	"yasko/helpers"
	"yasko/models"
)

type taskController struct{}

var Tasks = new(taskController)

func (tc taskController) Create(userID, platformID, authorityID string, themeID string, params []helpers.Parameter, platformCount int) (*models.Task, error) {
	var theme *models.Theme
	var authority *models.Authority
	var multiplier float64 = 1.0
	//Get user
	//get platform
	platform, err := models.Platforms.ByID(platformID)
	if err != nil {
		return nil, err
	}
	//get authority
	if authorityID != "" {
		authority, err = models.Authorities.ByID(authorityID)
		if err != nil {
			return nil, err
		}
		multiplier *= authority.Rate
	}

	//get theme
	if themeID != "" {
		theme, err = models.Themes.ByID(themeID)
		if err != nil {
			return nil, err
		}
		multiplier *= theme.Rate
	}
	parameters, err := models.Parameters.All()
	if err != nil {
		return nil, err
	}
	for _, parameter := range parameters {
		for _, param := range params {
			if param.ID == parameter.ID {
				if param.Active {
					multiplier *= parameter.Rate
				}
			}
		}
	}
	multiplier *= float64(platformCount)

	//calculate price
	price := platform.Price * multiplier

	//create task
	task, err := models.Tasks.Create(userID, platformID, authorityID, price, platformCount)
	return task, nil
}
