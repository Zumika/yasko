package api

type userResponse struct {
	ID          string  `json:"id"`
	Email       string  `json:"email"`
	AccessToken string  `json:"access_token"`
	Balance     float64 `json:"balance"`
	Active      bool    `json:"active"`
}

type parameterResponse struct {
	ID           string  `json:"id"`
	Title        string  `json:"title"`
	Availability bool    `json:"availability"`
	Active       bool    `json:"active"`
	Price        float64 `json:"price"`
	Rate         float64 `json:"rate"`
}

type taskResponse struct {
	ID            string  `json:"id"`
	UserID        string  `json:"user_id"`
	Active        bool    `json:"active"`
	Done          bool    `json:"done"`
	Price         float64 `json:"price"`
	PlatformID    string  `json:"platform_id"`
	AuthorityID   string  `json:"authority_id"`
	PlatformCount int     `json:"platform_count"`
}
