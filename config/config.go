package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"os"
)

type logger struct {
	Enabled bool `json:"enabled"`
}

type server struct {
	Address string `json:"address"`
	Url     string `json:"url"`
}

type config struct {
	Environment string `json:"environment"`
	SecretKey   string `json:"secret_key"`
	Logger      logger `json:"logger"`
	Mongo       string `json:"mongo"`
	Server      server `json:"server"`
}

var (
	Environment string
	Logger      logger
	Mongo       string
	Server      server
	SecretKey   string
)

func init() {
	parseConfig()
}

var Config = flag.String("config", "config.json", "path to config file")

func parseConfig() {
	var c config

	flag.Parse()

	var (
		jsonData []byte
		err      error
	)
	if *Config != "" {
		jsonData, err = ioutil.ReadFile(*Config)
		if err != nil {
			panic("could not read config from file")
		}
	} else if env := os.Getenv("CONFIG"); env != "" {
		jsonData = []byte(env)
	} else {
		panic("could not find config")
	}

	err = json.Unmarshal(jsonData, &c)
	if err != nil {
		panic(err)
	}

	Environment = c.Environment
	Logger = c.Logger
	Mongo = c.Mongo
	Server = c.Server
	SecretKey = c.SecretKey

}
