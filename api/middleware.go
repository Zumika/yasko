package api

import (
	"net/http"

	"errors"
	"yasko/models"
	"yasko/server"
)

func corsPreflight(c *server.Context) {
	c.Header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE")
	c.Header("Access-Control-Max-Age", "86400")
	c.Header("Access-Control-Allow-Credentials", "true")
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "Content-Type, Authorization")

	if c.Request.Method == "OPTIONS" {
		c.RenderStatus(http.StatusNoContent)
	}
}

func corsAfterflight(c *server.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
}

func requireAuth(f func(*server.Context, *models.User)) func(*server.Context) {
	return func(c *server.Context) {
		token := tokenFromRequest(c)
		user, err := models.Users.ByAccessToken(token)
		if err != nil {
			c.RenderJSON(http.StatusUnauthorized, JSONError(errors.New("Unauthorized")))
			return
		}
		f(c, user)
	}
}

func tokenFromRequest(c *server.Context) string {
	token := c.Request.Header.Get("Authorization")
	if token == "" {
		token = c.Request.URL.Query().Get("access_token")
	}
	return token
}
