package logger

import (
	"io/ioutil"
	"log"
	"os"

	"yasko/config"
)

var (
	// Debug represents a logger with the error level
	Debug *log.Logger
	// Info represents a logger with the info level
	Info *log.Logger
	// Error represents a logger with the error level
	Error *log.Logger
)

func init() {
	Debug = log.New(ioutil.Discard, "DEBUG: ", log.LstdFlags)
	Info = log.New(os.Stdout, "INFO: ", log.LstdFlags)
	Error = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)

	if config.Logger.Enabled {
		Debug.SetOutput(os.Stdout)
	}
}
