package models

import (
	"fmt"
	"gopkg.in/mgo.v2/bson"

	"yasko/config"
	"yasko/datastore"
	"yasko/helpers"
)

type users struct{}

var Users = new(users)

type User struct {
	ID          string  `bson:"_id"`
	Password    []byte  `bson:"password"`
	Email       string  `bson:"email"`
	AccessToken string  `bson:"access_token"`
	Balance     float64 `bson:"balance"`
	Active      bool    `bson:"active"`
}

func (u User) String() string {
	return fmt.Sprintf("User{ID:%v, Password:%v, Email:%v, AccessToken:%v, Active:%v}", u.ID, u.Password, u.Email, u.AccessToken, u.Active)
}

func (u users) Create(email, password string) (*User, error) {
	passwordHash, err := helpers.GeneratePassword(password, config.SecretKey)
	if err != nil {
		return nil, err
	}
	user := &User{
		ID:          helpers.GenerateID("user"),
		Password:    passwordHash,
		Email:       email,
		AccessToken: helpers.GenerateID("token"),
		Balance:     0.0,
		Active:      false,
	}
	return user, datastore.Mongo.Users.Insert(user)
}

func (u users) ByAccessToken(accessToken string) (*User, error) {
	var user *User
	return user, datastore.Mongo.Users.Find(bson.M{
		"access_token": accessToken,
	}).One(&user)
}

func (u users) ByEmail(email string) (*User, error) {
	var user *User
	return user, datastore.Mongo.Users.Find(bson.M{
		"email": email,
	}).One(&user)
}

func (u *User) GenerateToken() (string, error) {
	update := bson.M{}
	token := helpers.GenerateID("token")
	update["access_token"] = token
	return token, datastore.Mongo.Users.UpdateId(u.ID, bson.M{"$set": update})
}

func (u *User) Activate() error {
	update := bson.M{}
	update["active"] = true
	return datastore.Mongo.Users.UpdateId(u.ID, bson.M{"$set": update})
}
