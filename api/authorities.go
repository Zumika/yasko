package api

import (
	"net/http"

	"yasko/models"
	"yasko/server"
)

var authority = new(authorityAPI)

type authorityAPI struct{}

func handleAuthority(r *server.Router) {
	r.GET("", authority.show)
	r.POST("", authority.create)
	r.PUT("", authority.update)
	r.DELETE("", authority.remove)
}

func (aa *authorityAPI) show(c *server.Context) {
	objects, err := models.Authorities.All()
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.multiResponse(objects))
}

func (aa *authorityAPI) create(c *server.Context) {
	var req createParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Authorities.Create(req.Title, req.Availability, req.Price)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.response(object))
}

func (aa *authorityAPI) update(c *server.Context) {
	var req updateParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Authorities.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Update(req.Title, req.Availability, req.Price); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.response(object))
}

func (aa *authorityAPI) remove(c *server.Context) {
	var req removeRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Authorities.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Remove(); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
}

func (aa *authorityAPI) multiResponse(authorities []*models.Authority) []*parameterResponse {
	var pr []*parameterResponse
	for _, authority := range authorities {
		resp := aa.response(authority)
		if resp != nil {
			pr = append(pr, resp)
		}
	}
	return pr
}

func (aa *authorityAPI) response(p *models.Authority) *parameterResponse {
	return &parameterResponse{
		ID:           p.ID,
		Title:        p.Title,
		Availability: p.Availability,
		Rate:         p.Rate,
	}
}
