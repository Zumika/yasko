package datastore

import (
	mgo "gopkg.in/mgo.v2"
	"yasko/config"
)

type mongo struct {
	session     *mgo.Session
	db          *mgo.Database
	Users       *mgo.Collection
	Platforms   *mgo.Collection
	Tasks       *mgo.Collection
	Authorities *mgo.Collection
	Themes      *mgo.Collection
	Parameters  *mgo.Collection
}

// Mongo references a struct with all available collections
var Mongo *mongo

// Health checks if the DB is healty
func (m *mongo) Health() error {
	return m.session.Ping()
}

// Collection returns the collection by name
func (m *mongo) Collection(name string) *mgo.Collection {
	return m.db.C(name)
}

func init() {
	connectMongoDb()
	ensureIndexes()
}

func connectMongoDb() {
	if config.Mongo == "" {
		panic("no Mongo URL provided")
	}

	session, err := mgo.Dial(config.Mongo)
	if err != nil {
		panic(err)
	}
	db := session.DB("")

	Mongo = &mongo{
		session:     session,
		db:          db,
		Users:       db.C("users"),
		Platforms:   db.C("platforms"),
		Tasks:       db.C("tasks"),
		Authorities: db.C("authorities"),
		Themes:      db.C("themes"),
		Parameters:  db.C("parameters"),
	}
}

func ensureIndexes() {
	Mongo.Users.EnsureIndex(mgo.Index{Key: []string{"_id", "username"}, Unique: true})
	Mongo.Platforms.EnsureIndex(mgo.Index{Key: []string{"_id", "title"}, Unique: true})
	Mongo.Authorities.EnsureIndex(mgo.Index{Key: []string{"_id", "title"}, Unique: true})
	Mongo.Themes.EnsureIndex(mgo.Index{Key: []string{"_id", "title"}, Unique: true})
	Mongo.Tasks.EnsureIndex(mgo.Index{Key: []string{"_id"}, Unique: true})
	Mongo.Parameters.EnsureIndex(mgo.Index{Key: []string{"_id", "title"}, Unique: true})

}
