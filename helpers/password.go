package helpers

import (
	"golang.org/x/crypto/scrypt"
)

func GeneratePassword(password, secretKey string) ([]byte, error) {
	return scrypt.Key([]byte(password), []byte(secretKey), 16384, 8, 1, 32)
}
