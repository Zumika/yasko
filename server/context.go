package server

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"text/template"

	"github.com/gorilla/mux"
	"os"
	"yasko/logger"
	"time"
)

// Context wraps all necessary variables a handler needs
type Context struct {
	ResponseWriter http.ResponseWriter
	Request        *http.Request
	Map            map[string]interface{}
}

// Binding represents a parsed request body
type Binding interface {
	OK() error
}

// NewContext returns a new context given by the request
func NewContext(w http.ResponseWriter, r *http.Request) *Context {
	return &Context{ResponseWriter: w, Request: r, Map: map[string]interface{}{}}
}

//RenderFile
func (c *Context) RenderFile(status int, file *os.File) {
	cacheUntil := time.Now().AddDate(0, 0, 1).Format(http.TimeFormat)
	c.Header("Expires", cacheUntil)
	//MULTIPART!
	c.ResponseWriter.WriteHeader(status)
	io.Copy(c.ResponseWriter, file)
	c.log(status, "file")
}

// RenderJSON renders the given interface as JSON
func (c *Context) RenderJSON(status int, j interface{}) {
	c.Header("Content-Type", "application/json")
	c.ResponseWriter.WriteHeader(status)
	data, err := json.Marshal(j)
	if err != nil {
		c.setStatus(http.StatusInternalServerError)
		return
	}
	c.ResponseWriter.Write(data)
	c.log(status, "json")
}

// RenderTemplate renders the given template file and binding as HTML
func (c *Context) RenderTemplate(file string, binding interface{}) {
	t, _ := template.ParseFiles(file)
	t.Execute(c.ResponseWriter, binding)
}

// RenderError renders the given error and status code
func (c *Context) RenderError(status int, err error) {
	http.Error(c.ResponseWriter, err.Error(), status)
	c.log(status, err.Error())
}

// RenderStatus renders the given status code
func (c *Context) RenderStatus(status int) {
	http.Error(c.ResponseWriter, "", status)
	c.log(status, "null")
}

// RenderText renders the given text and status code
func (c *Context) RenderText(status int, text string) {
	c.Header("Content-Type", "text/plain")
	c.ResponseWriter.WriteHeader(status)
	c.ResponseWriter.Write([]byte(text))
	c.log(status, "text")
}

// RenderHTML renders the given HTMLs and status code
func (c *Context) RenderHTML(status int, html []byte) {
	c.Header("Content-Type", "text/plain")
	c.ResponseWriter.WriteHeader(status)
	c.ResponseWriter.Write(html)
	c.log(status, "html")
}

// Param returns the given param
func (c *Context) Param(name string) string {
	params := mux.Vars(c.Request)
	return params[name]
}

// ParamValues returns the given param
func (c *Context) ParamValues() map[string]string {
	return mux.Vars(c.Request)
}

// Query returns the given query parameter
func (c *Context) Query(key string) string {
	return c.Request.URL.Query().Get(key)
}

// QueryValues returns the raw query string
func (c *Context) QueryValues() url.Values {
	return c.Request.URL.Query()
}

// Set adds a value for a key to the map
func (c *Context) Set(key string, value interface{}) {
	c.Map[key] = value
}

// Get gets a value for a key from the map
func (c *Context) Get(key string) (interface{}, bool) {
	val, ok := c.Map[key]
	return val, ok
}

// Header sets the given header
func (c *Context) Header(name, value string) {
	c.ResponseWriter.Header().Set(name, value)
}

// Bind binds the request body to a struct by parsing the JSON
func (c *Context) Bind(obj Binding) error {
	if c.Request.Body != nil {
		defer c.Request.Body.Close()
		err := json.NewDecoder(c.Request.Body).Decode(obj)
		if err != nil && err != io.EOF {
			return err
		}
	}
	return obj.OK()
}

func (c *Context) setStatus(s int) {
	c.ResponseWriter.WriteHeader(s)
}

func (c *Context) log(status int, text string) {
	logger.Debug.Printf("API: %s %s - %d <%s> (%s)", c.Request.Method, c.Request.URL, status, text, c.Request.RemoteAddr)
}
