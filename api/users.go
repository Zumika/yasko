package api

import (
	"log"
	"net/http"

	"yasko/controllers"
	"yasko/logger"
	"yasko/models"
	"yasko/server"
)

var user = new(userAPI)

type userAPI struct{}

func handleUser(r *server.Router) {
	r.POST("/register", user.register)
	r.POST("/login", user.login)
	r.GET("/logout", requireAuth(user.logout))
	r.GET("/activate/{id}", user.activate)
	r.GET("/personal", requireAuth(user.show))
}

func (ua *userAPI) show(c *server.Context, user *models.User) {
	c.RenderJSON(http.StatusOK, ua.response(user))
}

func (ua *userAPI) register(c *server.Context) {
	var req createUserRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	user, err := controllers.Users.Register(req.Email)
	if err != nil {
		log.Print("Customer create troubles")
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, ua.response(user))
}

func (ua *userAPI) login(c *server.Context) {
	var req loginUserRequest
	if err := c.Bind(&req); err != nil {
		logger.Info.Print("Bind start")
		logger.Info.Print(err)
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	logger.Info.Print(req.Email, req.Password)
	token, err := controllers.Users.Login(req.Email, req.Password)
	if err != nil {
		c.RenderJSON(http.StatusUnauthorized, JSONError(err))
		return
	}
	logger.Info.Print(token, err)
	c.RenderJSON(http.StatusOK, map[string]string{"token": token})
}

func (ua *userAPI) logout(c *server.Context, user *models.User) {
	_, err := user.GenerateToken()
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, map[string]interface{}{"status": true})
}
func (ua *userAPI) activate(c *server.Context) {
	var req activateUserRequest
	if err := req.Parse(c.ParamValues()); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}

	user, err := models.Users.ByAccessToken(req.Token)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	err = user.Activate()
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, map[string]interface{}{"status": true})
}

func (ua *userAPI) response(u *models.User) *userResponse {
	return &userResponse{
		ID:          u.ID,
		Email:       u.Email,
		AccessToken: u.AccessToken,
		Active:      u.Active,
	}
}
