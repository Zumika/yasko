package helpers

import (
	"github.com/rs/xid"
)

func GenerateID(prefix string) string {
	id := xid.New().String()
	return prefix + id
}
