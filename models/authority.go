package models

import (
	"fmt"
	"gopkg.in/mgo.v2/bson"

	"yasko/datastore"
	"yasko/helpers"
)

type authorities struct{}

var Authorities = new(authorities)

type Authority struct {
	ID           string  `bson:"_id"`
	Title        string  `bson:"title"`
	Availability bool    `bson:"availability"`
	Rate         float64 `bson:"rate"`
}

func (a Authority) String() string {
	return fmt.Sprintf("Authority{ID:%v, Title:%v, Availability:%v, Price:%v}", a.ID, a.Title, a.Availability, a.Rate)
}

func (a authorities) Create(title string, availability bool, rate float64) (*Authority, error) {
	authority := &Authority{
		ID:           helpers.GenerateID("authority"),
		Title:        title,
		Availability: availability,
		Rate:         rate,
	}
	return authority, datastore.Mongo.Authorities.Insert(authority)
}

func (a authorities) ByID(id string) (*Authority, error) {
	var authority *Authority
	return authority, datastore.Mongo.Authorities.FindId(id).One(&authority)
}

func (a authorities) All() ([]*Authority, error) {
	var authorities []*Authority
	return authorities, datastore.Mongo.Authorities.Find(bson.M{}).All(&authorities)
}

func (a *Authority) Update(title string, availability bool, rate float64) error {
	update := bson.M{}
	if title != "" && title != a.Title {
		update["title"] = title
		a.Title = title
	}
	if availability != a.Availability {
		update["availability"] = availability
		a.Availability = availability
	}
	if rate != a.Rate {
		update["rate"] = rate
		a.Rate = rate
	}
	return datastore.Mongo.Authorities.UpdateId(a.ID, bson.M{"$set": update})
}

func (a *Authority) Remove() error {
	return datastore.Mongo.Authorities.RemoveId(a.ID)
}
