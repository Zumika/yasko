package api

import (
	"net/http"

	"yasko/models"
	"yasko/server"
)

var theme = new(themeAPI)

type themeAPI struct{}

func handleTheme(r *server.Router) {
	r.GET("", theme.show)
	r.POST("", theme.create)
	r.PUT("", theme.update)
	r.DELETE("", theme.remove)
}

func (aa *themeAPI) show(c *server.Context) {
	objects, err := models.Themes.All()
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.multiResponse(objects))
}

func (aa *themeAPI) create(c *server.Context) {
	var req createParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Themes.Create(req.Title, req.Availability, req.Price)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.response(object))
}

func (aa *themeAPI) update(c *server.Context) {
	var req updateParameterRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Themes.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Update(req.Title, req.Availability, req.Price); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	c.RenderJSON(http.StatusOK, aa.response(object))
}

func (aa *themeAPI) remove(c *server.Context) {
	var req removeRequest
	if err := c.Bind(&req); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	object, err := models.Themes.ByID(req.ID)
	if err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
	if err := object.Remove(); err != nil {
		c.RenderJSON(http.StatusBadRequest, JSONError(err))
		return
	}
}

func (aa *themeAPI) multiResponse(themes []*models.Theme) []*parameterResponse {
	var pr []*parameterResponse
	for _, theme := range themes {
		resp := aa.response(theme)
		if resp != nil {
			pr = append(pr, resp)
		}
	}
	return pr
}

func (aa *themeAPI) response(p *models.Theme) *parameterResponse {
	return &parameterResponse{
		ID:           p.ID,
		Title:        p.Title,
		Availability: p.Availability,
		Rate:         p.Rate,
	}
}
