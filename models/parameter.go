package models

import (
	"fmt"
	"gopkg.in/mgo.v2/bson"

	"yasko/datastore"
	"yasko/helpers"
)

type parameters struct{}

var Parameters = new(parameters)

type Parameter struct {
	ID     string  `bson:"_id"`
	Title  string  `bson:"title"`
	Active bool    `bson:"active"`
	Rate   float64 `bson:"rate"`
}

func (a Parameter) String() string {
	return fmt.Sprintf("Parameter{ID:%v, Title:%v, Active:%v, Price:%v}", a.ID, a.Title, a.Active, a.Rate)
}

func (a parameters) Create(title string, active bool, rate float64) (*Parameter, error) {
	parameter := &Parameter{
		ID:     helpers.GenerateID("parameter"),
		Title:  title,
		Active: active,
		Rate:   rate,
	}
	return parameter, datastore.Mongo.Parameters.Insert(parameter)
}

func (a parameters) ByID(id string) (*Parameter, error) {
	var parameter *Parameter
	return parameter, datastore.Mongo.Parameters.FindId(id).One(&parameter)
}

func (a parameters) All() ([]*Parameter, error) {
	var parameters []*Parameter
	return parameters, datastore.Mongo.Parameters.Find(bson.M{}).All(&parameters)
}

func (a *Parameter) Update(title string, active bool, rate float64) error {
	update := bson.M{}
	if title != "" && title != a.Title {
		update["title"] = title
		a.Title = title
	}
	if active != a.Active {
		update["active"] = active
		a.Active = active
	}
	if rate != a.Rate {
		update["rate"] = rate
		a.Rate = rate
	}
	return datastore.Mongo.Parameters.UpdateId(a.ID, bson.M{"$set": update})
}

func (a *Parameter) Remove() error {
	return datastore.Mongo.Parameters.RemoveId(a.ID)
}
